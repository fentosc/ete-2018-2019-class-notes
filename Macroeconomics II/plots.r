#Plot Indifference curves in line 125
u <- function(x, y) x*y

x <- seq(0, 10,0.1)
y <- seq(0, 10,0.1)
a <- c(15, 30)

pdf(file="fig1_1.pdf")
contour(x, y, outer(x, y, u), levels=a,bty="l",drawlabels=F,xlab="Money",ylab="Consumption",col=c("blue","blue"),lwd=c(2,2))
abline(0,1)
lines(c(sqrt(400*15)/10,0),c(0,sqrt(400*15)/10),col="red",lwd=2)
lines(c(sqrt(400*30)/10,0),c(0,sqrt(400*30)/10),col="red",lwd=2)
dev.off()

#Plot Lucasspec
x<-seq(-1,10,0.1)
y1<-1/x 
y2<-1/(x+1) + 1

pdf(file="lucasspec.pdf",width=7, height=4)
par(mfrow=c(1,2))
plot(x,y1,xlim=c(0,5),ylim=c(0,5),bty="l",type="l",xlab="Interest Rate",ylab="M1/Income",axes=F)
axis(1,at=0:5,labels=c("","",expression(hat(i)),expression(i),"",""),lwd.ticks=0)
axis(2,at=0:5,labels=F,lwd.ticks=0)
lines(c(2,2),c(-1,0.5),lty=2)
lines(c(3,3),c(-1,1/3),lty=2)
plot(x,y2,xlim=c(0,5),ylim=c(0,5),bty="l",type="l",xlab="Interest Rate",ylab="M1/Income",xaxt='n',yaxt='n')
dev.off()

#Plot IRF
x1<-rchisq(1000,10)
y1<-dchisq(x1,10)
x2<-rchisq(1000,5)
y2<-dchisq(x2,5)

pdf(file="irf.pdf")
plot(x1,-y1,pch=46,bty="l",xlim=c(0,25),ylim=c(-0.2,0),axes=F,xlab="Periods",ylab="Variation")
axis(2,labels=F,lwd.ticks=0)
axis(1,at=0:25,labels=F,lwd.ticks=0)
points(x2,-y2,pch=46)
dev.off()

#Prices change
x1<-seq(0.05,1,0.05)
p1<-rep(1,20)
p1[c(2,6)]<-0.5
p1[9:18]<-1.5
p1[c(10,13,15)]<-1.3
p1[18]<-0.3
p1[c(19,20)]<-0.4

x2<-seq(0.05,3,0.05)
p2<-rep(1,60)
p2[c(3,6,9,12,15,18)]<-0.8
p2[20:27]<-0.5
p2[28:35]<-0.7
p2[36:39]<-0.8
p2[c(43,46)]<-0.5
p2[50:55]<-0.3
p2[51]<-0.1
p2[53]<-0.4
p2[55]<-0.2
p2[56:60]<-1

pdf(file="priceschange.pdf",width=7, height=4)
par(mfrow=c(1,2))
plot(x1,p1,type="l",bty="l",xlab="Years",ylab="Price",axes=F,ylim=c(0,2))
axis(2,labels=F,lwd.ticks=0)
axis(1,at=c(0,1))

plot(x2,p2,type="l",bty="l",xlab="Years",ylab="Price",axes=F,ylim=c(0,2))
axis(2,labels=F,lwd.ticks=0)
axis(1,at=c(0,1,2,3))
lines(c(1,1),c(0,2),lty=2)
lines(c(2,2),c(0,2),lty=2)
lines(c(3,3),c(0,2),lty=2)
dev.off()

#672
pdf(file="pricemc.pdf",width=7,height=4)
plot(NULL,xlim = c(-2,5), ylim = c(-1,3))
abline(0,0,lty=2)
lines(c(-5,-1),c(-1,-1),col="blue")
lines(c(-5,-1),c(1,1),col="red")

lines(c(-1,0),c(-1,0.5),col="blue")
lines(c(-1,0),c(1,2.5),col="red")

lines(c(0,5),c(0.5,0.5),col="blue")
lines(c(0,5),c(2.5,2.5),col="red")

legend("topleft",col=c("blue","red"),legend=c("MC","Price"),lty=c(1,1),bty="n")
dev.off()